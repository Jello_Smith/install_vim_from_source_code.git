#!/bin/sh
CURRENT_DIR=$(pwd)
VIM_DIR=${CURRENT_DIR}/vim
VIM_INSTALL_DIR=${VIM_DIR}/install
VIM_BIN_DIR=${VIM_INSTALL_DIR}/bin

if [ -d ${VIM_INSTALL_DIR} ];then
	rm -rf ${VIM_INSTALL_DIR}
fi

#Get submodule vim
git submodule init
git submodule update

cd ${VIM_DIR}
./configure --prefix=${VIM_INSTALL_DIR} --with-tlib=tinfo --enable-pythoninterp=yes
make -j4
make install

cd ${VIM_BIN_DIR}
ln -s ${VIM_BIN_DIR}/vim ${VIM_BIN_DIR}/vi

. ${CURRENT_DIR}/setup_env.sh
