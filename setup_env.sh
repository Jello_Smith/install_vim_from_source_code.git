#!/bin/bash
CUR_DIR=`pwd`
PATH_ENV="PATH=${CUR_DIR}:\${PATH}"
BASHRC_FILE="${HOME}/.bashrc"

cur_dir_is_exist=`cat ${BASHRC_FILE} | grep "PATH" | grep "${CUR_DIR}"`
echo ${cur_dir_is_exist}

if [ "" = "${cur_dir_is_exist}" ];then
	echo ${PATH_ENV} >> ${BASHRC_FILE}
fi
